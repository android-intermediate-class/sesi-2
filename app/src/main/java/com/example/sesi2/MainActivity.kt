package com.example.sesi2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import com.example.sesi2.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), OnClickListener {

    private var _layout: ActivityMainBinding? = null

    private val layout: ActivityMainBinding
        get() = _layout ?: throw IllegalStateException("The activity has been destroyed")

    private var mHobbies: MutableSet<String> = mutableSetOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        _layout = binding
        setContentView(binding.root)

        layout.checkMovies.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                mHobbies.add("Movies")
            } else {
                mHobbies.remove("Movies")
            }
        }

        layout.checkBooks.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                mHobbies.add("Books")
            } else {
                mHobbies.remove("Books")
            }
        }

        layout.checkGames.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                mHobbies.add("Games")
            } else {
                mHobbies.remove("Games")
            }
        }

        layout.btnReset.setOnClickListener(this)
        layout.btnSubmit.setOnClickListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        _layout = null
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            layout.btnReset.id -> {
                layout.edtName.setText("")
                layout.edtPhone.setText("")
                layout.radGender.check(layout.radGenderMale.id)
                layout.checkMovies.isChecked = false
                layout.checkBooks.isChecked = false
                layout.checkGames.isChecked = false
            }

            layout.btnSubmit.id -> {
                layout.txtName.text = layout.edtName.text.toString()
                layout.txtPhone.text = layout.edtPhone.text.toString()

                val gender = when (layout.radGender.checkedRadioButtonId) {
                    layout.radGenderMale.id -> "Male"
                    layout.radGenderFemale.id -> "Female"
                    else -> ""
                }
                layout.txtGender.text = gender
                layout.txtHobbies.text = mHobbies.joinToString(", ")
            }
        }
    }
}